<!DOCTYPE html>
<html>
<head>
  <title>Girls in Tech Opportunities</title>
  <style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      padding: 5px;
    }
  </style>
</head>
<body>
  <h1>Girls in Tech Opportunities</h1>
  <table id="opportunitiesTable">
    <thead>
      <tr>
        <th>Title</th>
        <th>Link</th>
        <th>Snippet</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <script>
    fetch('/opportunities')
      .then(response => response.json())
      .then(data => {
        const opportunitiesTable = document.querySelector('#opportunitiesTable tbody');
        data.forEach(opportunity => {
          const row = document.createElement('tr');
          const titleCell = document.createElement('td');
          const linkCell = document.createElement('td');
          const snippetCell = document.createElement('td');
          titleCell.textContent = opportunity.title;
          linkCell.innerHTML = `<a href="${opportunity.link}">${opportunity.link}</a>`;
          snippetCell.textContent = opportunity.snippet;
          row.appendChild(titleCell);
          row.appendChild(linkCell);
          row.appendChild(snippetCell);
          opportunitiesTable.appendChild(row);
        });
      });
  </script>
</body>
</html>

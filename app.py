import csv
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import time
from apscheduler.schedulers.background import BackgroundScheduler

# Define the function that scrapes the data
def scrape_data():
    # Define the URL to scrape
    url = 'https://www.google.com/search?q=girls+in+tech+opportunities'

    # Send a GET request to the URL
    response = requests.get(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.content, 'html.parser')

    # Find all the search results on the page
    search_results = soup.find_all('div', {'class': 'g'})

    # Define the header row for the CSV file
    header = ['Title', 'Link', 'Snippet']

    # Open the CSV file for writing
    with open('girls_in_tech_opportunities.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # Write the header row to the CSV file
        writer.writerow(header)

        # Loop through each search result and extract the data
        for result in search_results:
            title = result.find('h3').get_text()
            link = result.find('a')['href']
            snippet = result.find('div', {'class': 's'}).get_text()

            # Write the data to the CSV file
            writer.writerow([title, link, snippet])

# Define the scheduler to run the scraper every day at a specified time
scheduler = BackgroundScheduler()
scheduler.add_job(scrape_data, 'interval', days=1, start_date=datetime.now() + timedelta(seconds=10))

# Start the scheduler
scheduler.start()

# Define the Flask app
app = Flask(__name__)

# Define the route for the API
@app.route('/girls-in-tech-opportunities')
def get_girls_in_tech_opportunities():
    # Read the data from the CSV file
    with open('girls_in_tech_opportunities.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        data = [row for row in reader]
        
    # Return the data as a JSON response
    return jsonify(data)

if __name__ == '__main__':
    app.run()
